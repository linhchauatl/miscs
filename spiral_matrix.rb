arr = [ 
        [ 1,  2,  3,  4,  5  ],
        [ 6,  7,  8,  9,  10 ],
        [ 11, 12, 13, 14, 15 ],
        [ 16, 17, 18, 19, 20 ],
      ]

def spiral_matrix(arr)
  output = []
  case_actions = {
    top:    lambda { arr.shift                },
    right:  lambda { arr.map(&:pop)           },
    bottom: lambda { arr.pop.reverse          },
    left:   lambda { arr.map(&:shift).reverse }
  }

  cases = case_actions.keys.cycle
  output += case_actions[cases.next].call until arr.empty?
  output
end

puts "\nResult is:"
puts spiral_matrix(arr).inspect