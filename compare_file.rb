first_file_name  = ARGV[0]
second_file_name = ARGV[1]


def read_content(file_name)
  f = File.new(file_name)
  content_gems = []
  while (st = f.gets)
    content_gems <<  st.split(/'|"/)[1]
  end
  f.close
  content_gems
end


first_gems  = read_content(first_file_name)
second_gems = read_content(second_file_name)

puts "\n\nGems that are in #{first_file_name} but not in #{second_file_name}"
(first_gems - second_gems).each do |name|
  puts name
end



