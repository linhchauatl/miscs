def combine_n(arr, n, result = [])
  return [] if arr.size < n
  if arr.size == n
    result << arr
    return result
  end

  0.upto(arr.size - 1) do |idx|
    sub_arr = arr.clone
    sub_arr.delete_at(idx)
    combine_n(sub_arr, n, result)
  end
  result.uniq
end

def max_product_of_3(arr)
  return nil if arr.size < 3
  return arr.inject(1) { |product, x| product *= x } if arr.size == 3

  combinations_3 = combine_n(arr, 3)

  result = nil
  combinations_3.each do |combination|
    product = combination.inject(1) { |product, x| product *= x }
    result = product if (result.nil? || result < product)
  end

  result
end

arr =  [1, 2, 3, 4, -5, 7, -3]
combine_n(arr, 3).sort.each { |x| puts x.inspect }
puts "\n\nMax product of any 3 elements is #{max_product_of_3(arr)}"