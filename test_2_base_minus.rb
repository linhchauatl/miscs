def power(number, pw)
  return 1 if pw == 0
  number *= power(number, pw - 1)
end

def to_decimal(bit_ar)
  decimal_value = 0
  bit_ar.each_with_index { |el, idx| decimal_value += el * power(-2, idx) }
  decimal_value
end

def solution(a)
  return [] if a.empty?
  raise 'Invalid bit sequence'  unless (a.reject { |el| el == 0 || el == 1 }).empty?

  decimal_value = to_decimal(a)

  puts("decimal_value = #{decimal_value}")

  result = []
  computed = 0
  begin
    div = decimal_value/(-2)
    result << decimal_value.abs % 2
    decimal_value = div
  end until div == 0 
  puts("inverse_value = #{to_decimal(result)}")
  result
end

puts(solution([1,0,0,1,1]).inspect)
puts("\n" * 3)
puts(solution([1,0,0,1,1,1]).inspect)
puts("\n" * 3)
puts(solution([1,0,1]).inspect)

