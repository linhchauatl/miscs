# Loop with upto
def pascal_triangle(n)
  row = [1]
  n.times do |i|
    puts row.join(' ')
    new_row = [1]
    0.upto(row.length - 2) { |i| new_row << row[i] + row[i+1] }
    new_row << 1
    row = new_row
  end
end

# Loop with each_with_index
def pascal_triangle(n, line = 1, row = [1])
  return if line > n

  puts row.join(' ')

  new_row = [1]
  row.each_with_index do |x, idx|
    new_row << x + row[idx + 1] if idx < (row.length - 1)
  end
  new_row << 1
  row = new_row
  pascal_triangle(n, line + 1, row)
end

# Recursive with for
def pascal_triangle(n, line = 1, row = [1])
  return if line > n

  puts row.join(' ')

  new_row = [1]
  for i in 0..(row.length - 2) do
    new_row << row[i] + row[i + 1]
  end

  new_row << 1
  row = new_row
  pascal_triangle(n, line + 1, row)
end
