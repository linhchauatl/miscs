=begin
       a
      / \
    b     c
  /\     / \
d  e    f   g
=end


class Node
  attr_accessor :left, :right, :value
  def initialize(value)
    @value = value
  end
end

def create_sample_tree
  root = Node.new('a')
  b = root.left  = Node.new('b')
  c = root.right = Node.new('c')
  b.left = Node.new('d')
  b.right = Node.new('e')
  c.left = Node.new('f')
  c.right = Node.new('g')
  root
end

def recursive_depth_search(root, value)
  return false if root.nil?
  puts root.value
  if root.value == value
    puts "Found #{root}: #{root.value}"
    return true
  end

  [root.left, root.right].each do |node|
    recursive_depth_search(node, value)
  end

  false
end

def loop_depth_search(root, value)
  return false if root.nil?
  queue = [root]

  while node = queue.shift do
    puts node.value
    if node.value == value
      puts "Found #{node}: #{node.value}"
      return true
    end

    queue.unshift(node.right) if node.right
    queue.unshift(node.left)  if node.left
  end

  false
end

root = create_sample_tree
['a', 'g', 'x'].each do |value|
  puts "Recursive Search for #{value}: "
  puts recursive_depth_search(root, value)
  puts '-' * 5
end

puts "\n" * 3

['a', 'g', 'x'].each do |value|
  puts "Non recursive Search for #{value}: "
  puts loop_depth_search(root, value)
  puts '-' * 5
end
