# The "Island Count" Problem

# Given a 2D matrix M, filled with either 0s or 1s, count the number of islands of 1s in M.
# An island is a group of adjacent values that are all 1s. Every cell in M can be adjacent to the 4 cells that are next to it on the same row or column.

# Explain and code the most efficient solution possible and analyze its runtime complexity.

# Example: the matrix below has 6 islands:
#         0  1  0  1  0
#         0  0  1  1  1
#         1  0  0  1  0
#         0  1  1  0  0
#         1  0  1  0  1

arr = [ [0, 1, 0, 1, 0], 
        [0, 0, 1, 1, 1],
        [1, 0, 0, 1, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 1, 0, 1] ]

def toggle(arr, row, column)
  [ [row, column - 1], [row,column + 1], [row - 1, column], [row + 1, column] ].each do |pos|
    next if !( pos[0].between?(0, arr.size - 1)  && pos[1].between?(0, arr[pos[0]].size - 1) && arr[pos[0]][pos[1]] == 1 )      
    arr[pos[0]][pos[1]] = 2
    toggle(arr, pos[0], pos[1])
  end
end

def island_count(arr)
  count = 0
  for row in 0...(arr.size) do
    for column in 0...(arr[row].size) do
      next if arr[row][column] != 1
      count += 1
      arr[row][column] = 2
      toggle(arr, row, column)
    end
  end
  count
end

puts island_count(arr)