class Utility
  class << self
    def create_array(str)
      return [] if str.to_s.strip == ''
      str.split(/\W+/).reverse.each_with_object([]){|w, a| a.unshift([w, a.length+1])}
    end

    def create_hash(arr)
      return {} if (!arr.is_a? Array) || (arr.select { |x| !x.is_a?(Array) || (x.size != 2) }).any?
      arr.inject({}) { |result, x| result.merge( {x[0] => x[1]} ) }
    end
    
    # Credit : https://www.facebook.com/the.real.scott.holden
    def string_to_hash(str)
      return {} if str.to_s.strip == ''
      Hash[str.split(/\W+/).reverse.each_with_object([]){|w, a| a.unshift([w, a.length+1])}]
    end 

  end

end

# Ruby 1.9 Hash['Hi I am Rob'.split.reverse.each_with_object([]){|w, a| a.unshift([w, a.length+1])}]
# Ruby 1.8 Hash[ 'Hi I am Rob'.split.inject([]){|a, w| a.unshift([w, a.length+1]); a} ]
# Ruby 1.9, convert string to hash: Hash['Hi, I am Rob'.split(/\W+/).reverse.each_with_object([]){|w, a| a.unshift([w, a.length+1])}]
# Hash[[["Hi", 4], ["I", 3], ["am", 2], ["Rob", 1]]]   |=>  {"Hi"=>4, "I"=>3, "am"=>2, "Rob"=>1} 