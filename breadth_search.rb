=begin
       a
      / \
    b     c
  /\     / \
d  e    f   g
=end


class Node
  attr_accessor :left, :right, :value
  def initialize(value)
    @value = value
  end
end

def create_sample_tree
  root = Node.new('a')
  b = root.left  = Node.new('b')
  c = root.right = Node.new('c')
  b.left = Node.new('d')
  b.right = Node.new('e')
  c.left = Node.new('f')
  c.right = Node.new('g')
  root
end

def breath_search(root, value)
  return false if root.nil?

  queue = [root]
  while node = queue.shift do
    puts node.value

    if node.value == value
      puts "Found #{node}: #{node.value}"
      return true
    else
      queue << node.left
      queue << node.right
    end
  end

  false
end

root = create_sample_tree
['a', 'g', 'x'].each do |value|
  puts "Search for #{value}: "
  puts breath_search(root, value)
  puts '-' * 5
end
