1.upto 100 do |i|
  print 'fizz ' if i % 3 == 0
  print 'buzz' if i % 5 == 0
  print i if i % 3 != 0 and i % 5 != 0
  puts
end  