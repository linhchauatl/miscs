lines = ARGV[0].to_i

# Algorithm 1 - use printed_number variable to hold the value of numbers that must be printed
def print_triangle(lines)
  printed_number = 1
  1.upto lines do |line|
    line.times do
      print "#{printed_number} "
      printed_number +=1
    end
    puts
  end
end

# Algorithm 2 - use a recursive function to calculate the start number of each line
def print_triangle2(lines)
  1.upto lines do |line|
    line.times do |i|
      print "#{start_of_line(line) + i} "
    end
    puts
  end
end

def start_of_line(line)
  return 1 if line == 1
  return start_of_line(line - 1) + line - 1
end


# Verify to see if the 2 algorithms match each others
print_triangle(lines)
puts
print_triangle2(lines)