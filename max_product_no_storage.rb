def max_product_of_n(arr, n, result = nil)
  return {} if arr.size < n
  if arr.size == n
    product = arr.inject(1) { |product, x| product *= x }
    result = { array: arr, value: product } if (result.nil? || result[:value] < product)
    return result
  end

  0.upto(arr.size - 1) do |idx|
    sub_arr = arr.clone
    sub_arr.delete_at(idx)
    result = max_product_of_n(sub_arr, n, result)
  end
  result
end

arr =  [1, 2, 3, 4, -5, 7, -3]
max_product = max_product_of_n(arr, 3)
puts "Max product of any 3 elements of #{arr.inspect} is #{max_product[:array].join(' * ')} = #{max_product[:value]}\n"


puts "Utilize Ruby Array#combination method:"
result = nil
arr.combination(3) do |combination|
  product = combination.inject(1) { |product, x| product *= x }
  result = { array: combination, value: product } if (result.nil? || result[:value] < product)
end

puts "Result is #{result[:array].join(' * ')} = #{result[:value]}"
