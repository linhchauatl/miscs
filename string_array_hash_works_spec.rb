require "#{File.dirname(__FILE__)}/string_array_hash_works"
describe Utility do
  context 'create_array' do
    it 'should return an empty array if the argument is nil' do
      Utility.create_array(nil).should == []
    end

    it 'should return an empty array if the string is empty' do
      Utility.create_array('').should == []
    end

    it 'should return an array of arrays with [word, reversed index] if the string has content' do
      Utility.create_array('Hi, I am Rob.').should == [ ['Hi', 4], ['I', 3], ['am', 2], ['Rob', 1] ]
    end
  end

  context 'create_hash' do
    it 'should return an empty hash if the argument is nil' do
      Utility.create_hash(nil).should == {}
    end

    it 'should return an empty hash if the array is not an array of 2-element arrays' do
       Utility.create_hash([ ['Hi', 4, 3], 'I', ['am', 2], ['Rob', 1] ]).should == {}
    end

    it 'should return a hash with format { x[0] => x[1], y[0] => y[1] } if the array is an array of 2-element arrays' do
      Utility.create_hash([ ['Hi', 4], ['I', 3], ['am', 2], ['Rob', 1] ]).should == { 'Hi' => 4, 'I'=> 3, 'am'=> 2, 'Rob' => 1 }
    end
  end

  context 'string_to_hash' do
    it 'should return an empty hash if the argument is nil' do
      Utility.string_to_hash(nil).should == {}
    end

    it 'should return an empty hash if the string is empty' do
       Utility.string_to_hash('').should == {}
    end

    it 'should return a hash with format { x[0] => x[1], y[0] => y[1] } if the string has content' do
      Utility.string_to_hash('Hi, I am Rob.').should == { 'Hi' => 4, 'I'=> 3, 'am'=> 2, 'Rob' => 1 }
    end
  end
end