def two_sum_slow(nums, target)
  Struct.new('Number', :value, :index)

  ords = []
  0.upto(nums.size - 1) do |i|
    ords << Struct::Number.new(nums[i], i)
  end

  ords.sort! { |x, y| x.value <=> y.value }
  left  = 0
  right = ords.size - 1
  while left < right
    sum = ords[left].value + ords[right].value
    return [ords[left].index, ords[right].index ] if sum == target

    if sum < target
      left += 1
    else
      right -= 1
    end
  end

end


def two_sum_faster(nums, target)

  ords = []
  0.upto(nums.size - 1) do |i|
    ords << [nums[i], i]
  end

  ords.sort! { |x, y| x[0] <=> y[0] }
  left  = 0
  right = ords.size - 1
  while left < right
    sum = ords[left][0] + ords[right][0]
    return [ords[left][1], ords[right][1] ] if sum == target

    if sum < target
      left += 1
    else
      right -= 1
    end
  end

end


def binary_insert(sorted_arr, value)
  return [value] if sorted_arr.empty?

  left = 0
  right = sorted_arr.size - 1

  if sorted_arr[left][0] > value[0]
    sorted_arr.unshift(value)
    return sorted_arr
  elsif sorted_arr[right][0] < value[0]
    sorted_arr << value
    return sorted_arr
  end

  middle = right / 2

  while left < right do
    if sorted_arr[middle][0] < value[0]
      left = middle + 1
      middle = (left + right) / 2
    elsif sorted_arr[middle][0] > value[0]
      right = middle - 1
      middle = (left + right) / 2
    else
      sorted_arr.insert(middle, value)
      return sorted_arr
    end
  end

  index = (sorted_arr[middle][0] < value[0])? middle + 1 : middle
  sorted_arr.insert(index, value)
  sorted_arr
end

def two_sum(nums, target)
  ords = []
  0.upto(nums.size - 1) do |i|
    ords = binary_insert( ords, [nums[i], i] )
  end

  left  = 0
  right = ords.size - 1
  while left < right
    sum = ords[left][0] + ords[right][0]
    return [ords[left][1], ords[right][1] ] if sum == target

    if sum < target
      left += 1
    else
      right -= 1
    end
  end
end

puts two_sum([2, 7, 11, 15], 9).inspect
puts two_sum([0,4,3,0], 0).inspect
