# The "Island Count" Problem

# Given a 2D matrix M, filled with either 0s or 1s, count the number of islands of 1s in M.
# An island is a group of adjacent values that are all 1s. Every cell in M can be adjacent to the 4 cells that are next to it on the same row or column.

# Explain and code the most efficient solution possible and analyze its runtime complexity.

# Example: the matrix below has 6 islands:
#         0  1  0  1  0
#         0  0  1  1  1
#         1  0  0  1  0
#         0  1  1  0  0
#         1  0  1  0  1

arr = [ [0, 1, 0, 1, 0], 
        [0, 0, 1, 1, 1],
        [1, 0, 0, 1, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 1, 0, 1] ]

def toggle(arr, row, column)
  arr[row][column] = 2
  [[row, column - 1], [row,column + 1], [row - 1, column], [row + 1, column]].each do |pos|
    arr[pos[0]][pos[1]] = 2 if ( arr[pos[0]]&.[](pos[1]) == 1 ) 
  end
end

def island_count(arr)
  count = 0
  stack = []
  arr.each_with_index do |row, row_idx|
    row.each_with_index do |element, column_idx|
      next if element != 1
      count += 1
      current_column = column_idx
      stack << [row_idx, current_column] while arr[row_idx][current_column += 1 ] == 1
      current_row = row_idx
      stack << [current_row, column_idx] while arr[current_row += 1]&.[](column_idx) == 1     
      stack.each { |position| toggle(arr, position[0], position[1]) }
    end
  end
  count
end
       
puts island_count(arr)
